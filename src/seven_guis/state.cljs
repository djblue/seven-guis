(ns seven-guis.state
  (:refer-clojure :exclude [atom])
  (:require ["react" :as react]
            [reagent.core :as r]))

(def state-context (react/createContext nil))

(defn with-state [state & children]
  (into [:r> (.-Provider state-context) #js {:value state}] children))

(defn use-state [] (react/useContext state-context))

(def atom-context (react/createContext nil))

(defn atom [value]
  ((or (react/useContext atom-context) r/atom) value))

(defn with-atom [a & children]
  (into [:r> (.-Provider atom-context) #js {:value #(do (reset! a %) a)}] children))