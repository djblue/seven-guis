(ns seven-guis.dom
  (:require [seven-guis.theme :as theme]))

(defn input [props]
  (let [theme (theme/use-theme)
        style (cond-> {:padding (:padding theme)
                       :border-radius (:border-radius theme)
                       :border (str "1px solid " (:color/border theme))
                       :color (:color/text theme)
                       :background (:color/button theme)}
                (:disabled props)
                (assoc :cursor :not-allowed :opacity 0.5)
                (:error props)
                (assoc :border "1px solid #BF616A"))]
    [:input (update props :style #(merge style %))]))

(defn select [props & children]
  (let [theme (theme/use-theme)
        style {:padding (:padding theme)
               :border-radius (:border-radius theme)
               :border (str "1px solid " (:color/border theme))
               :color (:color/text theme)
               :background (:color/button theme)}]
    (into [:select (update props :style #(merge style %))] children)))

(defn button [& args]
  (let [[props & children] args
        theme (theme/use-theme)
        style (cond-> {:padding-left (* 2 (:padding theme))
                       :padding-right (* 2 (:padding theme))
                       :cursor :pointer
                       :padding-top (:padding theme)
                       :padding-bottom (:padding theme)
                       :border-radius (:border-radius theme)
                       :border (str "1px solid " (:color/border theme))
                       :color (:color/text theme)
                       :background (:color/button theme)}
                (:disabled props)
                (assoc :cursor :not-allowed :opacity 0.5))]
    (into [:button
           (if-not (map? props)
             {:style style}
             (update props :style #(merge style %)))]
          (if (map? props) children args))))