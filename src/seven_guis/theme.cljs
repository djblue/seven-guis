(ns seven-guis.theme
  (:require ["react" :as react]))

(def theme-context (react/createContext nil))

(defn with-theme [theme & children]
  (into [:r> (.-Provider theme-context) #js {:value theme}] children))

(defn use-theme [] (react/useContext theme-context))

(def colors
  {:dark
   {:color/border "#4c566a"
    :color/button "#272c36"
    :color/text   "#d8dee9"
    :color/bg     "#2e3440"}
   :darker
   {:color/border "#4c566a"
    :color/button "#2e3440"
    :color/text   "#d8dee9"
    :color/bg     "#272c36"}
   :light
   {:color/border "#d8dee9"
    :color/button "#eceff4"
    :color/text   "#4c566a"
    :color/bg     "#ffffff"}})

(def theme
  {:padding 4
   :gap 7
   :border-radius 2})

(defn get-theme [color] (merge theme (get colors color)))