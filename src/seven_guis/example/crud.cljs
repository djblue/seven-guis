(ns seven-guis.example.crud
  (:require [clojure.string :as str]
            [reagent.core :as r]
            [seven-guis.dom :as d]
            [seven-guis.state :as s]
            [seven-guis.theme :as theme]))

(defn ->number [^js input]
  (let [n (js/parseFloat (.-value (.-target input)))]
    (when-not (js/isNaN n) n)))

(defn prefix? [s substr]
  (or (str/blank? substr)
      (str/starts-with?
       (str/lower-case s)
       (str/lower-case (str/trim substr)))))

(defn crud []
  (r/with-let [state (s/atom {:filter-text ""
                              :selected nil
                              :name ""
                              :surname ""
                              :db [{:name "Emil" :surname "Hans"}
                                   {:name "Mustermann" :surname "Max"}
                                   {:name "Tisch" :surname "Roman"}]})]
    (let [{:keys [filter-text selected name surname db]} @state
          theme (theme/use-theme)]
      [:div
       {:style
        {:display :grid
         :gap (:gap theme)
         :align-items :center
         :grid-template-columns "100px auto"}}

       [:div "Filter prefix: "]
       [d/input {:value filter-text
                 :on-change #(swap! state assoc :filter-text (.-value (.-target %)))}]

       [d/select
        {:style
         {:grid-column "1/3"
          :overflow :auto}
         :multiple true
         :value (if-let [id selected] [id] [])
         :on-change #(swap! state assoc :selected (->number %))}
        (map-indexed
         (fn [id {:keys [name surname]}]
           (when (prefix? surname filter-text)
             [:option {:key id :value id} (str name ", " surname)]))
         db)]

       [:div "Name: "]
       [d/input
        {:value name
         :on-change #(swap! state assoc :name (.-value (.-target %)))}]

       [:div "Surname: "]
       [d/input
        {:value surname
         :on-change #(swap! state assoc :surname (.-value (.-target %)))}]

       [:div
        {:style
         {:grid-column "1/3"
          :display :grid
          :gap (:gap theme)
          :grid-template-columns "1fr 1fr 1fr"}}
        [d/button
         {:on-click (fn []
                      (swap! state update :db conj {:name name :surname surname}))}
         "Create"]
        [d/button
         {:disabled (nil? selected)
          :on-click (fn []
                      (swap! state assoc-in [:db selected] {:name name :surname surname}))}
         "Update"]
        [d/button
         {:disabled (nil? selected)
          :style (when (nil? selected) {:opacity 0.5})
          :on-click (fn []
                      (swap! state
                             assoc
                             :db (filterv some? (assoc db selected nil))
                             :selected nil))}
         "Delete"]]])))

(def example
  {:title "CRUD"
   :challenge "separating the domain and presentation logic, managing mutation, building a non-trivial layout."
   :component crud})