(ns seven-guis.example.temperature-converter
  (:require [reagent.core :as r]
            [seven-guis.dom :as d]
            [seven-guis.state :as s]
            [seven-guis.theme :as theme]))

(defn round [n]
  (.round js/Math (/ (* n 100) 100)))

(defn ->celsius [{:keys [unit value]}]
  (when value
    (if (= unit :celsius) value (round (* (- value 32) (/ 5 9))))))

(defn ->fahrenheit [{:keys [unit value]}]
  (when value
    (if (= unit :fahrenheit) value (round (+ 32 (* value (/ 9 5)))))))

(defn ->number [^js input]
  (let [n (js/parseFloat (.-value (.-target input)))]
    (when-not (js/isNaN n) n)))

(defn temperature-converter  []
  (r/with-let [state (s/atom {})]
    (let [temperature @state
          theme (theme/use-theme)]
      [:div
       {:style
        {:display :grid
         :grid-template-columns "auto 1fr"
         :gap (:gap theme)
         :align-items :center}}
       [:div "Celsius"]
       [d/input
        {:type :number
         :value (->celsius temperature)
         :on-change #(reset! state {:unit :celsius :value (->number %)})}]
       [:div]
       [:div {:style {:text-align :center}} "="]
       [:div "Fahrenheit"]
       [d/input
        {:value (->fahrenheit temperature)
         :on-change #(reset! state {:unit :fahrenheit :value (->number %)})}]])))

(def example
  {:title "Temperature Converter"
   :challenge "bidirectional data flow, user-provided text input."
   :component temperature-converter})
