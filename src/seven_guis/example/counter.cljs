(ns seven-guis.example.counter
  (:require [reagent.core :as r]
            [seven-guis.dom :as d]
            [seven-guis.state :as s]
            [seven-guis.theme :as theme]))

(defn counter []
  (r/with-let [counter (s/atom 0)]
    (let [theme (theme/use-theme)]
      [:div
       {:style {:display :flex :gap (:gap theme)}}
       [:span @counter]
       [d/button {:on-click #(swap! counter inc)} "Count"]])))

(def example
  {:title "Counter"
   :challenge "Understanding the basic ideas of a language/toolkit."
   :component counter})