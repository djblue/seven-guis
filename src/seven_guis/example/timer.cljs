(ns seven-guis.example.timer
  (:require ["react" :as react]
            [reagent.core :as r]
            [seven-guis.dom :as d]
            [seven-guis.state :as s]))

(defn ->number [^js input]
  (let [n (js/parseFloat (.-value (.-target input)))]
    (when-not (js/isNaN n) n)))

(defn timer []
  (r/with-let [state (s/atom {:timer 0 :timeout 15})]
    (let [{:keys [timer timeout]} @state]
      (react/useEffect
       (fn []
         (let [interval (js/setInterval #(swap! state update :timer + 100) 100)]
           #(js/clearInterval interval))))
      [:div
       {:style
        {:display :grid
         :gap 5
         :align-items :center
         :grid-template-columns "auto 1fr"}}
       [:div "Elapsed Time: "]
       [:progress {:max 1 :style {:width "100%"} :value (/ timer (* 1000 timeout))}]

       [:div]
       [:div timeout "s"]

       [:div "Duration: "]
       [d/input {:type :range
                 :min 0.1
                 :max 30
                 :step 0.1
                 :value timeout
                 :on-change #(swap! state assoc :timeout (->number %))}]

       [d/button
        {:style {:grid-column "1/3"}
         :on-click #(swap! state assoc :timer 0)} "Reset Timer"]])))

(def example
  {:title "Timer"
   :challenge "concurrency, competing user/signal interactions, responsiveness."
   :component timer})