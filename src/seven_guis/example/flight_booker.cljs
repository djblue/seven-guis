(ns seven-guis.example.flight-booker
  (:require [reagent.core :as r]
            [seven-guis.dom :as d]
            [seven-guis.state :as s]
            [seven-guis.theme :as theme]))

(defn date? [date-string]
  (and (some? date-string)
       (re-matches #"\d{4}-\d{2}-\d{2}" date-string)))

(defn valid? [{:keys [flight start end]}]
  (case flight
    "one-way" (and (date? start) (nil? end))
    "return"  (and (date? start)
                   (date? end)
                   (<= start end))))

(def ^:private date-length (count "YYYY-mm-dd"))
(defn today [] (subs (.toISOString (js/Date.)) 0 date-length))

(defn flight-booker []
  (r/with-let [state (s/atom {:flight "one-way" :start (today)})]
    (let [{:keys [flight start end]} @state
          theme (theme/use-theme)
          valid (valid? @state)
          one-way? (= flight "one-way")]
      [:div
       {:style
        {:display :flex
         :gap (:gap theme)
         :flex-direction :column}}
       [d/select {:value flight
                  :style {:padding (:padding theme)}
                  :on-change (fn [e]
                               (let [flight (.-value (.-target e))]
                                 (swap! state
                                        (fn [state]
                                          (cond-> (assoc state :flight flight)
                                            (= flight "one-way")
                                            (dissoc :end))))))}
        [:option {:value "one-way"} "one-way flight"]
        [:option {:value "return"} "return flight"]]
       [d/input
        {:type :date
         :value start
         :error (when (not (date? start))
                  "Not a complete date.")
         :on-change #(swap! state assoc :start (.-value (.-target %)))}]
       [d/input
        {:type :date
         :value end
         :error (when (not one-way?) (not (date? end))
                      "Not a complete date.")
         :on-change #(swap! state assoc :end (.-value (.-target %)))
         :disabled one-way?}]
       [d/button
        {:disabled (not valid)
         :on-click (fn []
                     (js/alert
                      (str "You have booked a " flight
                           " flight that departs on " start
                           (when end
                             (str " and returns on " end))
                           ".")))}
        "Book"]])))

(def example
  {:title "Flight Booker"
   :challenge "Constraints."
   :component flight-booker})