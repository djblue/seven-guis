(ns seven-guis.example
  (:require [clojure.pprint :as pp]
            [reagent.core :as r]
            [seven-guis.dom :as d]
            [seven-guis.state :as s]
            [seven-guis.theme :as theme]))

(defn example [{:keys [title component]}]
  (r/with-let [state (r/atom nil)]
    [:<>
     [:h2
      {:style
       {:margin-top 20
        :margin-bottom 0
        :font-family :sans-serif}}
      title]
     [:div]
     [:div
      {:style
       {:display :flex
        :align-items :center
        :justify-content :center}}
      [:div
       {:style {:width "80%"}}
       [s/with-atom state [component]]]]
     [:pre 
      {:style {:white-space :pre-wrap}}
      (with-out-str (pp/pprint @state))]]))

(defn color-selector []
  (let [state (s/use-state)]
    [d/select
     {:value (name (:colors @state))
      :on-change #(swap! state assoc :colors (keyword (.-value (.-target %))))}
     (for [color (map name (keys theme/colors))]
       [:option {:key color :value color} color])]))

(defn examples [examples-components]
  (let [theme (theme/use-theme)]
    [:div
     {:style
      {:color (:color/text theme)
       :background (:color/bg theme)
       :min-height "100vh"
       :display :flex
       :flex-direction :column
       :justify-content :center}}

     [:div
      {:style
       {:margin "0 auto"
        :width 800
        :gap 10
        :display :grid
        :grid-template-columns "1fr 1fr"}}
      [:h1 {:style
            {:margin 0
             :font-family :sans-serif}}
       [:s "7"] "5GUIs"]
      [color-selector]

      (for [e examples-components]
        ^{:key (:title e)} [example e])]]))