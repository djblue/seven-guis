(ns seven-guis.core
  (:require [reagent.core :as r]
            [reagent.dom :as dom]
            [seven-guis.example :as example]
            [seven-guis.example.counter :as counter]
            [seven-guis.example.crud :as crud]
            [seven-guis.example.flight-booker :as flight-booker]
            [seven-guis.example.temperature-converter :as temperature-converter]
            [seven-guis.example.timer :as timer]
            [seven-guis.state :as s]
            [seven-guis.theme :as theme]))

(def examples-components
  [counter/example
   temperature-converter/example
   flight-booker/example
   timer/example
   crud/example])

(defn app []
  (r/with-let [state (r/atom {:colors :light})]
    [s/with-state
     state
     [theme/with-theme
      (theme/get-theme (:colors @state))
      [example/examples examples-components]]]))

(def functional-compiler (r/create-compiler {:function-components true}))

(defn render-app []
  (dom/render [app]
              (.getElementById js/document "root")
              functional-compiler))

(defn main! [] (render-app))

(defn reload! [] (render-app))