# Seven GUIs

Implements 5 of the 7GUIs found [here][guis] that test various UI concepts.

## Dev

To get a development REPL started, do the following:

```bash
npm install
clojure -M:cljs:shadow:dev:test:repl
```

To view the examples, go to [http://localhost:4400](http://localhost:4400)

[guis]: https://eugenkiss.github.io/7guis/tasks/