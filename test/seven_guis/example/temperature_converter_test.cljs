(ns seven-guis.example.temperature-converter-test
  (:require [clojure.test :refer [deftest is]]
            [seven-guis.example.temperature-converter :as tc]))

(deftest conversions
  (is (= 1 (tc/->celsius {:unit :celsius :value 1})))
  (is (= 5 (tc/->celsius {:unit :fahrenheit :value 41})))
  (is (= 1 (tc/->fahrenheit {:unit :fahrenheit :value 1})))
  (is (= 41 (tc/->fahrenheit {:unit :celsius :value 5}))))