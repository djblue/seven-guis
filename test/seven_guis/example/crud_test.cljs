(ns seven-guis.example.crud-test
  (:require [clojure.test :refer [deftest are]]
            [seven-guis.example.crud :as c]))

(deftest prefix-filtering
  (are [prefix]
       (true? (c/prefix? "Hans" prefix))
    ""
    "   "
    "h"
    "H"
    "hans"
    "Hans"
    " Hans ")
  (are [prefix]
       (false? (c/prefix? "Hans" prefix))
    "ans"
    "  ans"))