(ns seven-guis.example.flight-booker-test
  (:require [clojure.test :refer [deftest are is testing]]
            [seven-guis.example.flight-booker :as fb]))

(deftest valid-flights
  (are [title state]
       (testing title (true? (fb/valid? state)))

    "one-way should only have a start date"
    {:flight "one-way" :start "2023-03-09"}

    "one-way flights should have a start date"
    {:flight "one-way" :start "2023-03-09"}

    "return flights should not have an end date before start"
    {:flight "return" :start "2023-03-09" :end "2023-03-09"}))

(deftest invalid-flights
  (are [title state]
       (testing title (false? (fb/valid? state)))

    "one-way should only have a start date"
    {:flight "one-way" :start "2023-03-09" :end "2023-03-09"}

    "one-way flights should have a start date"
    {:flight "one-way" :end "2023-03-09"}

    "return flights should not have an end date before start"
    {:flight "return" :start "2023-03-09" :end "2023-03-08"}))